package com.tsconsulting.dsubbotin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tsconsulting.dsubbotin.tm.util.DateAdapter;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractFasterXmlDataCommand extends AbstractDataCommand {

    public ObjectMapper getObjectMapper(ObjectMapper objMapper) {
        @NotNull final ObjectMapper objectMapper = objMapper;
        objectMapper.setDateFormat(DateAdapter.getSimpleDateFormat());
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        return objMapper;
    }

}
